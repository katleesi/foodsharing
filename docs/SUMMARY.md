# Summary

- [Overview](README.md)
- [Aufruf zur Unterstützung (DE)](it-tasks.md)
- [Call for contribution (EN)](it-tasks_EN.md)

### Getting started
  - [Getting the code](getting-the-code.md)
  - [Running the code: Docker](running-the-code.md)
  - [Setting things up](setting-things-up.md)

### Contributing
  - [Einführung in Git und unser Arbeiten](contributing_DE.md)
  - [Making and submitting changes](contributing.md)
  - [getting to know our codebase](codebase.md)
  - [Code style](codestyle.md)
  - [Testing](testing.md)
  - [Code quality and review](code-review.md)
  - [Rebase](rebase.md)
  - [Troubleshooting](troubleshooting.md)
  - [Beta Testing (DE)](beta-testing-de.md)

### References
  - [Scripts](scripts.md)
  - [php](php.md)
    - [Translation](internationalization.md)
  - [request types](requests.md)
  - [javascript, vue.js](javascript.md)
    - [icons](icons.md)
  - [Databases](database.md)
  - [Tests](tests.md)
  - [Refactoring](refactor.md)
  - [Glossary](GLOSSARY.md)

- [About the devdocs](about-devdocs.md)
- [Contributors](contributors.md)
